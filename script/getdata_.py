from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException
import datetime
import sys
import json
import os

# rpc_user and rpc_password are set in the bitcoin.conf file
rpc_user='faircoin'
rpc_password='NU5QsSmUGL-onUzeosSvpbb23mUiH8xS'

rpc_connection = AuthServiceProxy("http://%s:%s@127.0.0.1:8333"%(rpc_user, rpc_password))

# check datafiles blockcount
fn=os.environ['HOME'] + '/data/txs.info.json'
if not os.path.isfile(fn):
    file = open(fn,'w')
    file.write(json.dumps({ 'blockhash' : 'beed44fa5e96150d95d56ebd5d2625781825a9407a5215dd7eda723373a0a1d7', 'timestamp' : int( datetime.datetime.now().timestamp() ) }))
    file.close()

file = open(fn,'r')
txs_info=json.loads( file.read() )
file.close()

tstamp=txs_info['timestamp']

# check if updateflow is already running
if tstamp + 60 > datetime.datetime.now().timestamp():
    sys.exit(0)

block_hash=txs_info['blockhash']
last_blocks=txs_info['lastblocks']

# update time
fn=os.environ['HOME'] + '/data/txs.info.json'
file = open(fn,'w')
file.write(json.dumps({ 'blockhash' : block_hash, 'timestamp' : int( datetime.datetime.now().timestamp() ), 'lastblocks' : txs_info['lastblocks'] }))
file.close()

commands = [ [ "getblock", block_hash ] ]
block = rpc_connection.batch_(commands)[0]

if not os.path.exists(os.environ['HOME'] + '/data/tx'):
    os.makedirs(os.environ['HOME'] + '/data/tx')

while 'nextblockhash' in block:
    block_hash=block['nextblockhash']
    commands = [ [ "getblock", block_hash ] ]
    block = rpc_connection.batch_(commands)[0]
    year = datetime.datetime.fromtimestamp(block['time']).isoformat()[0:4]

    if not os.path.exists(os.environ['HOME'] + '/data/tx/' + year ):
        os.makedirs(os.environ['HOME'] + '/data/tx/' + year )

    fn=os.environ['HOME'] + '/data/tx/' + year + '/' + datetime.datetime.fromtimestamp(block['time']).isoformat()[0:10]+'.yml'
    file = open(fn,'a')
    file.write( '- ' + str( len( block['tx'] ) ) + '\n' );
    file.close()

    txs_info['lastblocks'].append( [ block['height'], len( block['tx'] )-1 ] )
    if len( txs_info['lastblocks'] ) > 480:
        txs_info['lastblocks'].pop(0)

    fn=os.environ['HOME'] + '/data/txs.info.json'
    file = open(fn,'w')
    file.write(json.dumps({ 'blockhash' : block_hash, 'timestamp' : int( datetime.datetime.now().timestamp() ), 'lastblocks' : txs_info['lastblocks'] }))
    file.close()
