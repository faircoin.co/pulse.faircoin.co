from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException
import datetime
import sys
import json
import os

# rpc_user and rpc_password are set in the bitcoin.conf file
rpc_user=os.environ['rpcuser']
rpc_password=os.environ['rpcpassword']
rpc_connect=os.environ['rpcconnect']
rpc_port=os.environ['rpcport']
script_path=os.path.dirname(os.path.abspath(__file__))

rpc_connection = AuthServiceProxy("http://%s:%s@%s:%s"%(rpc_user, rpc_password, rpc_connect, rpc_port ))

# check datafiles blockcount
fn=script_path+'/../_data/faircoin/txs.info.json'
if not os.path.isfile(fn):
    file = open(fn,'w')
    file.write(json.dumps({ 'blockhash' : 'beed44fa5e96150d95d56ebd5d2625781825a9407a5215dd7eda723373a0a1d7', 'timestamp' : int( datetime.datetime.now().timestamp() ), 'lastblocks' : [] }))
    file.close()

file = open(fn,'r')
txs_info=json.loads( file.read() )
file.close()

tstamp=txs_info['timestamp']

# check if updateflow is already running
if tstamp + 60 > datetime.datetime.now().timestamp():
    sys.exit(0)

block_hash=txs_info['blockhash']
last_blocks=txs_info['lastblocks']

# update time
fn=script_path+'/../_data/faircoin/txs.info.json'
file = open(fn,'w')
file.write(json.dumps({ 'blockhash' : block_hash, 'timestamp' : int( datetime.datetime.now().timestamp() ), 'lastblocks' : txs_info['lastblocks'] }))
file.close()

commands = [ [ "getblock", block_hash ] ]
block = rpc_connection.batch_(commands)[0]

if not os.path.exists(script_path+'/../_data/faircoin/tx'):
    os.makedirs(script_path+'/../_data/faircoin/tx')

while 'nextblockhash' in block:
    block_hash=block['nextblockhash']
    commands = [ [ "getblock", block_hash ] ]
    block = rpc_connection.batch_(commands)[0]
    year = datetime.datetime.fromtimestamp(block['time']).isoformat()[0:4]

    if not os.path.exists(script_path+'/../_data/faircoin/tx/' + year ):
        os.makedirs(script_path+'/../_data/faircoin/tx/' + year )

    fn=script_path+'/../_data/faircoin/tx/' + year + '/' + datetime.datetime.fromtimestamp(block['time']).isoformat()[0:10]+'.yml'
    file = open(fn,'a')
    file.write( '- ' + str( len( block['tx'] ) ) + '\n' );
    file.close()

    txs_info['lastblocks'].append( [ block['height'], len( block['tx'] )-1 ] )
    if len( txs_info['lastblocks'] ) > 480:
        txs_info['lastblocks'].pop(0)

    fn=script_path+'/../_data/faircoin/txs.info.json'
    file = open(fn,'w')
    file.write(json.dumps({ 'blockhash' : block_hash, 'timestamp' : int( datetime.datetime.now().timestamp() ), 'lastblocks' : txs_info['lastblocks'] }))
    file.close()
