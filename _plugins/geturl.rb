require 'open-uri'

module GetUrl
  def get_by_url(url)
    open(url).read
  end

  def convert_xml(x)
    JSON.load(Hash.from_xml(x).to_json)
  end

end

Liquid::Template.register_filter(GetUrl) # register filter globally
