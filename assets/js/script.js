var chart;
var pulse;
var tstamp=0;
var data_pulse;

function getByTag(id){
  switch(id){
    case 'tx_pulse':
      changeUrl(id);
    case undefined:
      unloadChart();
      loadChartTxPulse();
    break;
    case 'tx_chart':
      unloadChart();
      loadChartTx();
      changeUrl(id);
    break;
  }
}

function changeUrl(id){
  // manage url params
  ChangeUrl( '',  (( id != '' ) ? '?tag=' : '' )+id );
}

function ChangeUrl(page, query ) {
    if( query == '' ){
      query=window.location.pathname;
    }
    if (typeof (history.replaceState) != "undefined") {
        var obj = { Page: page, Url: query };
        history.replaceState(obj, obj.Page, obj.Url);
    } else {
        alert("Browser does not support HTML5.");
    }
}

function unloadChart(){
  clearTimeout(pulse);
  $('.legend').html('');
}

function getQueryVariable(variable){
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    if(pair[0] == variable){return pair[1];}
  }
}

function upd_chart(){
  clearTimeout( pulse );
  pulse=setTimeout(
    function(){
      data_pulse=content_load('data/faircoin/txs.lastblocks.csv','csv');
      chart.load( { rows: data_pulse } );
      $('.legend span:first').html( 'blockheight<br>' + parseInt( data_pulse.slice(-1)[0] ) );
      $('.legend span:last').html( 'tx / last 24h<br>' + getDailyTx() );
      upd_chart();
    }, 30000
  );
}

function getDailyTx(){
  var tx_daily=0;
  data_pulse.slice(1).forEach(
    function(v,i){
      tx_daily+=1*v[1];
    }
  );
  return tx_daily;
}

function loadChartTx(){

  var D=content_load('data/faircoin/txs.csv','csv');
  var data_chart=[];
  var avg=[];
  D[0].push('avg_tx');
  data_chart.push( D[0] );
  D.slice(1).forEach(
    function(v,i){
      avg.push(v[2]);
      if( avg.length > 30 ) avg.shift();
      v.push( parseInt( avg.reduce(function(a, b){return parseInt(a)+parseInt(b);}) / avg.length ) );
      data_chart.push(v);
    }
  );

  chart = c3.generate({
      bindto: '#chart',
      subchart: { show: true },
      point: { r: 0 },
      line: { width: 2 },
      bar: { width: 2 },
      zoom: { enabled : true },
      data: {
        rows: data_chart,
        x: 'date',
        colors: { blocks: 'grey', real_tx: 'midnightblue', 'avg_tx' : 'goldenrod', 'log' : 'blue', 'linear' : 'grey' },
        types: { real_tx: 'bar' },
        axes: {
          blocks: 'y2'
        },
        order: null
      },
      axis: {
        x: {
          type: 'timeseries',
          tick: { format: '%y-%m-%d',
            values: [
              '2017-07-01',
              '2017-10-01',
              '2018-01-01',
              '2018-04-01',
              '2018-07-01',
              '2018-10-01',
              '2019-01-01',
              '2019-04-01',
              '2019-07-01',
              '2019-10-01',
              '2020-01-01',
              '2020-04-01',
              '2020-07-01',
              '2020-10-01'
            ] }
        },
        y: {
          //tick: { format: d3.format('.0f'), values: [ 0, 10, 100, 1000] },
          tick: { format: d3.format('.0f') },
          type: 'log',
          min: 0,
          padding: { bottom: 0 }
        },
        y2: {
          show: true
        }
      }
  });

  d3.select('.orders').insert('div', '.chart').attr('class', 'legend').selectAll('span')
  .data(['linear', 'log'])
  .enter().append('span')
  .attr('data-id', function (id) { return id; })
  .html(function (id) { return id; })
  .each(function (id) {
      d3.select(this).style('background-color', chart.color(id));
  })
  .on('click', function (id) {
      //chart.toggle(id);
      chart.axis.types({ 'y' : id });
      d3.selectAll('span').style('background-color', 'grey' );
      d3.select(this).style('background-color', 'blue' );
  });

}

function loadChartTxPulse(){

  data_pulse=content_load('data/faircoin/txs.lastblocks.csv','csv');
  chart = c3.generate({
      bindto: '#chart',
      subchart: { show: false },
      point: { r: 0 },
      bar: { width: 2 },
      zoom: { enabled : false },
      data: {
        rows: data_pulse,
        colors: { block_tx: 'midnightblue', 'tx' : 'midnightblue', 'blockheight' : 'goldenrod' },
        types: { block_tx: 'line' },
        x: 'block'
      },
      axis: {
        x: {
          type: 'linear',
          tick: { }
        },
        y: {
          //tick: { format: d3.format('.0f'), values: [ 0, 10, 100, 1000] },
          tick: { format: d3.format('.0f') },
          type: 'linear',
          min: 0,
          max: 10,
          padding: { bottom: 10 }
        }
      }
  });

  d3.select('.orders').insert('div', '.chart').attr('class', 'legend').selectAll('span')
  .data(['blockheight', 'tx'])
  .enter().append('span')
  .attr('data-id', function (id) { return id; })
  .html(function (id) { return id; })
  .each(function (id) {
      d3.select(this).style('background-color', chart.color(id));
  });

  $('.legend span:first').html( 'blockheight<br>' + parseInt( data_pulse.slice(-1)[0] ) );
  $('.legend span:last').html( 'tx / last 24h<br>' + getDailyTx() );
  upd_chart();
}
