function content_load( url, datatype ){
  var json = null;
  var result= '';
  if( datatype == undefined ){
    datatype=url.match(/\..*$/g)[0].slice(1);
  }
  $.ajax({
      'async': false,
      'global': false,
      'cache': false,
      'url': url,
      'dataType': ((datatype == 'csv') ? 'text' : datatype),
      'success': function (data) {
          result = data;
      }
  });

  if(datatype == 'csv'){
    result=result.split(/\n/g);
    var AA=[];
    result.forEach(
      function(v,i){
        var A=v.split(/,/g);
        if( A.length > 1 && A[0] != '' ){
          AA.push(A);
        }
      }
    );
    result=AA;
  }

  return result;
}

function getQueryVariable(variable)
{
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}
