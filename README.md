# FairCoin Pulse

Follow the pulse of FairCoin!

Live and static faircoin numbers.

The service is made for/controlled by gitlab runner.

## Usage

Go into gitlab **CI/CD** -> **Pipeline** and **Run Pipeline**

Enter variable name **CMD**

#### CMD - commands

~~~
build        # build container ( changes of Dockerfile )
start        # start container ( changes of scripts )
stop         # stop container
uninstall    # remove container
~~~

#### CI/CD Settings

Go Gitlab **Settings** -> **CI/CD** -> **Variables**

~~~
#### FairCoin.Co group variables ######################
FAIRCOIN_CONF        # faircoin.conf file
LH_PORT_pulse        # jekyll serve -p
~~~

## Development <small>( manual usage )</small>

If you want create an instance manual then follow the  instructions.

1. install docker and docker-compose ( https://docs.docker.com/compose/install/ )
2. clone this project
3. change configuration in ./env
4. run services by ./control.sh

~~~
chmod +x ./control.sh
./control.sh build
./control.sh start
./control.sh stop
./control.sh uninstall
~~~
